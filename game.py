import os
import math
import sys, pygame
from operator import add


pygame.init()


class Cube(object):
    def __init__(self, image, x=0, y=0):
        super(Cube, self).__init__()

        self.image = image
        self.sprite = pygame.image.load(image).convert_alpha()

        self.position = [x, y]

    def set_position(self, value):
        self.position = value

    def move(self, position):
        self.position = [sum(x) for x in zip(self.position, position)]

    def get_rect(self):
        return self.sprite.get_rect()

    def set_image(self, image):
        self.sprite = pygame.image.load(image).convert_alpha()


class Ball(Cube):
    def __init__(self, x=0, y=0):
        super(Ball, self).__init__(os.path.join('assets', 'ball_black.png'), x, y)


class Cursor(Cube):
    def __init__(self, x=0, y=0):
        super(Cursor, self).__init__(os.path.join('assets', 'bumper.png'), x, y)


def get_brick_state(resistance, kicks):
    if resistance >= 3:
        # 0 or 1 hit => state 1
        # 2 or 3 hits => state 2
        # 4 or 5 hits => state 3
        state = math.floor((kicks + 1) / resistance) + 1
        return state if state <= 3 else 3
    if resistance == 2:
        return 1
    if resistance <= 1:
        return 1

    return resistance * 2


def get_brick_image(resistance, kicks):
    image = 'square_blue_state{state}.png'
    if resistance == 2:
        image = 'square_yellow_state{state}.png'
    if resistance >= 3:
        image = 'square_red_state{state}.png'

    new_state = get_brick_state(resistance, kicks)

    return os.path.join('assets', image.format(state=new_state))


class Brick(Cube):
    def __init__(self, resistance, x=0, y=0):
        brick_image = get_brick_image(resistance, 0)
        super(Brick, self).__init__(brick_image, x, y)

        self.resistance = resistance
        self.kicks = 0

    def is_alive(self):
        return self.kicks >= self.resitance

    def increment_kicks(self):
        self.kicks = self.kicks + 1

        new_brick_image = get_brick_image(self.resistance, self.kicks)
        self.set_image(new_brick_image)


class Cursor(Cube):
    def __init__(self, x=0, y=0):
        super(Cursor, self).__init__(os.path.join('assets', 'bumper.png'), x, y)


class Life(Cube):
    def __init__(self, x=0, y=0):
        super(Life, self).__init__(os.path.join('assets', 'life.png'), x, y)


class OldGameEngine():
    def __init__(self, *args, **kwargs):
        self.balls = []

    def register_ball(self, ball):
        """
            ball:
        """
        self.balls.append(ball)

    def game_loop(self):
        while 1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT: sys.exit()

            # Ball management
            for ball in self.balls:
                ball_rect = ball.position
                ball_rect = [sum(x) for x in zip(ball_rect, speed)]

                ball.set_position(ball_rect)

                screen.blit(ball.sprite, ball_rect)

            pygame.display.flip()
            # pygame.time.delay(10)


class GameEngine():
    elements = []

    def __init__(self, screen_size):
        self.screen = pygame.display.set_mode(screen_size)

        background_image = os.path.join('assets', 'background.png')
        self.background = pygame.image.load(background_image)

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        self.screen.blit(self.background, (0, 0))

    def register_elements(self, elements):
        self.elements = elements

    def render(self, items):
        for item_key, item_value in items.items():
            self.screen.blit(item_value.sprite, item_value.position)

        pygame.display.flip()
        # If delay is needed
        # pygame.time.delay(10)
