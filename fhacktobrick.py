from game import GameEngine
from game import Brick, Cursor, Ball, Life


screen_size = width, height = 1280, 960
engine = GameEngine(screen_size)

b1 = Ball()
b2 = Ball(452, 98)
b3 = Ball(56, 17)
b4 = Ball(866, 700)

cursor = Cursor(50, 250)

brick_1_1 = Brick(1, 98, 634)
brick_1_2 = Brick(1, 148, 634)
brick_1_3 = Brick(1, 198, 634)

brick_2_1 = Brick(2, 98, 400)
brick_2_2 = Brick(2, 148, 400)
brick_2_3 = Brick(2, 198, 400)

brick_3_1 = Brick(3, 98, 200)
brick_3_2 = Brick(3, 148, 200)
brick_3_3 = Brick(3, 198, 200)

life_1 = Life(60, 700)
life_2 = Life(60, 760)
life_3 = Life(60, 860)

items = {
    'ball1': b1,
    'ball2': b2,
    'ball3': b3,
    'ball4': b4,

    'cursor': cursor,

    'brick1_1': brick_1_1,
    'brick1_2': brick_1_2,
    'brick1_3': brick_1_3,
    'brick2_1': brick_2_1,
    'brick2_2': brick_2_2,
    'brick2_3': brick_2_3,
    'brick3_1': brick_3_1,
    'brick3_2': brick_3_2,
    'brick3_3': brick_3_3,

    'life_1': life_1,
    'life_2': life_2,
    'life_3': life_3,
}

speed1 = [10, 10]
speed2 = [10, 10]
speed3 = [10, 10]

count = 0


def compute_game(objects):
    global count
    objects['ball1'].move(speed1)
    objects['ball2'].move(speed2)
    objects['ball3'].move(speed3)

    if count >= 20:
        objects['brick3_1'].increment_kicks()
        count = 0

    print(count)

    count = count + 1

    return objects


while 1:
    engine.handle_events()
    items = compute_game(items)
    engine.render(items)
